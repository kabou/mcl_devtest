local C = minetest.colorize

local formspec = table.concat({
	"formspec_version[4]",
	"size[9,9.5]",
	"style_type[label;font_size=25;font=bold]",
})

local colors = {
	"BLACK",
	"DARK_BLUE",
	"DARK_GREEN",
	"DARK_AQUA",
	"DARK_RED",
	"DARK_PURPLE",
	"GOLD",
	"GRAY",
	"DARK_GRAY",
	"BLUE",
	"GREEN",
	"AQUA",
	"RED",
	"LIGHT_PURPLE",
	"YELLOW",
	"WHITE",
}

local count = 0
for _,name in pairs(colors) do
	count = count + 1
	formspec = formspec..
		"label[0.4,".. 0.4 + 0.5 * count..";"..C(mcl_colors[name], "mcl_colors."..name).."]"..
		"box[6,".. 0.2 + 0.5 * count..";2.5,0.5;"..mcl_colors.background[name].."]"
end

minetest.register_chatcommand("colors", {
	description = "Show a formspec featuring all color in mcl_colors",
	func = function(playername)
		minetest.show_formspec(playername, "mcl_test_colors:colors", formspec)
	end,
})