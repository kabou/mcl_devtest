# abmp - abm profiler

Dumps a list of the abms and how long they each took last round.

## Chatcommands

/abmp - dump report
