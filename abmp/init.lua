local count={}
local times={}
local max_times={}
local total=0
local nabm=0
minetest.register_on_mods_loaded(function()

	for k,v in pairs(minetest.registered_abms) do
		local n=v.label
		local olda=v.action
		minetest.registered_abms[k].action=function(pos, node, aoc, aocw)
			local t1=os.clock()
			local r=olda(pos, node, aoc, aocw)
			times[n]=os.clock()-t1
			if times[n] > ( max_times[n] or 0 ) then max_times[n] = times[n] end
			if not count[n] then count[n] = 0 end
			count[n]=count[n]+1
			total=total+times[n]
			nabm=nabm+1
			return r
		end
	end
end)

minetest.register_chatcommand("abmp",{privs={debug=true},
	description="Give an overview of the registered abms and how long they each take",
	func=function(p)
	table.sort(count)
	table.sort(times)
	minetest.chat_send_player(p,"counts:"..dump(count))
	minetest.chat_send_player(p,"=== times:"..dump(times))
	minetest.chat_send_player(p,"=== max times:"..dump(max_times))
	minetest.chat_send_player(p,"total abm time last interval:"..dump(total))
	minetest.chat_send_player(p,"total abms registered:"..dump(#times))
end})
