
local orenodes = {
	"mcl_core:stone_with_diamond",
	"mcl_core:stone_with_iron",
	"mcl_core:stone_with_gold",
	"mcl_core:stone_with_coal",
	"mcl_core:stone_with_lapis",
	"mcl_core:stone_with_emerald",
	"mcl_core:stone_with_redstone",
	"mcl_core:stone_with_redstone_lit",
	"mcl_nether:ancient_debris",
	"mcl_blackstone:blackstone_gilded",
	"mcl_blackstone:nether_gold",
}

mcl_info.register_debug_field("Ores near",{
	level = 4,
	func = function(pl,pos)
		local p1 = vector.offset(pos,-32,-32,-32)
		local p2 = vector.offset(pos,32,32,32)
		local ores, counts = minetest.find_nodes_in_area(p1,p2,orenodes)
		local r = {}
		for k,v in pairs(counts) do
			if v > 0 then r[k]=v end
		end
		return dump(r)
	end
})
