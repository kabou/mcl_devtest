local S = minetest.get_translator("mcl_burning")


minetest.register_chatcommand("mgflags", {
	params = "",
	description = "Get Mapgen flags",
	privs = {debug = true},
	func = function(name)
		minetest.chat_send_player(name,dump(minetest.get_mapgen_setting("mg_flags")))
	end
})

minetest.register_chatcommand("biome", {
	params = "",
	description = "Gets current biome",
	privs = {debug = true},
	func = function(name)
		local player=minetest.get_player_by_name(name)
		local biome=minetest.get_biome_data(player:get_pos())
		local bname = minetest.get_biome_name(biome.biome)
		minetest.chat_send_player(name,bname.. " " ..dump(biome))
	end
})

minetest.register_chatcommand("burn", {
	params = S("<playername> <duration> <reason>"),
	description = S("Sets a player on fire for the given amount of seconds with the given reason."),
	privs = { debug = true },
	func = function(name, params)
		local playername, duration, reason = params:match("^(.+) (.+) (.+)$")
		if not (playername and duration and reason) then
			return false, S("Error: Parameter missing.")
		end
		local player = minetest.get_player_by_name(playername)
		if not player then
			return false, S(
				"Error: Player “@1” not found.",
				playername
			)
		end
		local duration_number = tonumber(duration)
		-- Lua numbers are truthy
		-- NaN is not equal to NaN
		if not duration_number or (duration_number ~= duration_number) then
			return false, S(
				"Error: Duration “@1” is not a number.",
				duration
			)
		end
		if duration_number < 0 then
			return false, S(
				"Error: Duration “@1” is negative.",
				duration
			)
		end
		mcl_burning.set_on_fire(
			player,
			duration_number,
			reason
		)
		return true, S(
			"Set @1 on fire for @2s for the following reason: @3",
			playername,
			duration,
			reason
		)
	end,
})
